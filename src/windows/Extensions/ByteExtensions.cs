﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;

namespace CANBusLogger.Extensions;

public static class ByteExtensions
{
    public static byte[] FromHexString(this string input)
    {
        if (string.IsNullOrEmpty(input))
            return null;

        if (input.Length % 2 != 0)
            return null;

        input = input.Replace(" ", "");
        return Enumerable.Range(0, input.Length)
            .Where(x => x % 2 == 0)
            .Select(x => Convert.ToByte(input.Substring(x, 2), 16))
            .ToArray();
    }

    public static string ToHexString(this byte[] input)
    {
        if (input == null || input.Length < 1)
            return string.Empty;

        return string.Join("", Enumerable.Range(0, input.Length)
            .Select(x => $"{input[x]:X2}"));
    }
}