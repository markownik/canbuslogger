﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Windows;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using CANBusLogger.Models;

namespace CANBusLogger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly SerialPort _serialPort;
        private Task _backgorundTask = null;
        private CanBusMessageCollection _messages = new CanBusMessageCollection();

        public MainWindow()
        {
            InitializeComponent();

            _serialPort = new SerialPort()
            {
                BaudRate = 115200,
                Parity = Parity.None,
                StopBits = StopBits.One,
                DataBits = 8,
                DtrEnable = false
            };

            ComPortOpenClose.Click += ComPortOpenCloseOnClick;
            OutputClear.Click += delegate(object sender, RoutedEventArgs args) { _messages.Clear(); };
            CanMsgGrid.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = _messages });
            _messages.CollectionChanged += MessagesOnCollectionChanged;
        }

        private void MessagesOnCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            if (CanMsgGrid.Items.Count > 0 && (AutoScroll.IsChecked ?? false))
            {
                var border = VisualTreeHelper.GetChild(CanMsgGrid, 0) as Decorator;
                if (border != null)
                {
                    var scroll = border.Child as ScrollViewer;
                    if (scroll != null) scroll.ScrollToEnd();
                }
            }
        }

        private async void ComPortOpenCloseOnClick(object sender, RoutedEventArgs e)
        {
            if (_serialPort.IsOpen) // close port
            {
                _serialPort.Close();
                if (_backgorundTask != null)
                {
                    await _backgorundTask.WaitAsync(CancellationToken.None);
                    _backgorundTask.Dispose();
                }
                ComPortOpenClose.Content = "Open";
            }
            else // open port
            {
                _serialPort.PortName = ComPortName.Text;

                try
                {
                    _serialPort.Open();
                    _serialPort.DiscardInBuffer();
                    ComPortOpenClose.Content = "Close";
                    _backgorundTask = Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            while (_serialPort.IsOpen)
                            {
                                var msg = new CanBusMessage(_serialPort.ReadLine().Trim());
                                Dispatcher.BeginInvoke(() => { _messages.AddOrUpdate(msg); });
                            }
                        }
                        catch {}
                    });
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"error opening port: {ex.Message} {ex.StackTrace}");
                }
            }
        }
    }
}
