﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Threading;

namespace CANBusLogger.Models;

public class CanBusMessageCollection : ObservableCollection<CanBusMessage>
{
    private readonly List<uint> _messages = new List<uint>();

    public void AddOrUpdate(CanBusMessage msg)
    {
        if (_messages.Contains(msg.Id))
        {
            var item = Items.Single(m => m.Id == msg.Id);
            var index = Items.IndexOf(item);
            item.Update(msg);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, item, index, index));
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, item, index));
        }
        else
        {
            _messages.Add(msg.Id);
            Add(msg);
        }
    }

    protected override void ClearItems()
    {
        _messages.Clear();
        base.ClearItems();
    }
}