﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CANBusLogger.Annotations;
using CANBusLogger.Extensions;

namespace CANBusLogger.Models;

public class CanBusMessage : INotifyPropertyChanged
{
    public uint Id { get; private set; }

    public byte[] Data { get; private set; }

    public string DataString => Data.ToHexString();

    public CanBusMessage(string input)
    {
        var msg = input.Split('|');
        Id = Convert.ToUInt32(msg[0], 16);
        Data = msg[1].FromHexString();
    }

    public CanBusMessage(uint id, byte[] data)
    {
        Id = id;
        Data = data;
    }

    public void Update(CanBusMessage msg)
    {
        if (msg.Id == Id)
        {
            Data = msg.Data;
            OnPropertyChanged(nameof(Data));
            OnPropertyChanged(nameof(DataString));
        }
    }

    public override string ToString()
    {
        return $"Id: {Id} Data: {Data.ToHexString()}";
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    [NotifyPropertyChangedInvocator]
    private void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}