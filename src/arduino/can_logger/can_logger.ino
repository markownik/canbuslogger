#include <StreamLib.h>
#include <stdint.h>
#include <SPI.h>
#include <df_can.h>
#include <df_candfs.h>

bool DEBUG = false;

const int SPI_CS_PIN = 10;

char buff[128];
BufferedPrint bp(Serial, buff, sizeof(buff));
MCPCAN CAN(SPI_CS_PIN); 

uint32_t id;
uint8_t len = 0;
uint8_t buf[8];

void setup() {
    Serial.begin(115200);
    while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB
    }
    
    int count = 5; // the max numbers of initializint the CAN-BUS, if initialize failed first!.
    do {
        CAN.init();   
        if(CAN_OK == CAN.begin(CAN_1000KBPS))
        {
            bp.printf("%08X|%02X", 0, 0);
            bp.flush();
            Serial.println();
            break;
        }
        else
        {
            bp.printf("%08X|%02X", 0, 1);
            bp.flush();
            Serial.println();
            delay(100);
        }
        
    } while(count--);
    
    if (count < 1)
    {
        bp.printf("%08X|%02X", 0, 2);
        bp.flush();
        Serial.println();
    }
       
    if(DEBUG)
        randomSeed(analogRead(0));

    delay(2000);
}

void loop() {
    if(CAN_MSGAVAIL == CAN.checkReceive() || DEBUG)
    {
        if(DEBUG)
        {
            delay(5);
            id = random(100, 200);
            len = random(1, 8);
            for(int i = 0; i < len; i++)
            {
              buf[i] = random(0, 255);
            }
        }
        else
            CAN.readMsgBufID(&id, &len, buf);   
        
        bp.printf("%08X|", id);
        for(int i = 0; i<len; i++)
            bp.printf("%02X", buf[i]);
        bp.flush();
        Serial.println();
    }
}
